var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var stylus = require('stylus');
var cors = require('cors');

var conf = require('./config/serverconf');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var crewRouter = require('./routes/crew');
var projectsRouter = require('./routes/projects');
var creditsRouter = require('./routes/credits');
var stagesRouter = require('./routes/stages');
var sheetsRouter = require('./routes/sheets');

var db = require('./db');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

db.connect(conf.mongo_url);

app.use(cors());

app.use(conf.base_url + '', indexRouter);
app.use(conf.base_url + '/users', usersRouter);
app.use(conf.base_url + '/crew', crewRouter);
app.use(conf.base_url + '/projects', projectsRouter);
app.use(conf.base_url + '/credits', creditsRouter);
app.use(conf.base_url + '/stages', stagesRouter);
app.use(conf.base_url + '/sheets', sheetsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
