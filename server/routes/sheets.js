var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var db = require('../db');
var moment = require('moment');
var CrewModel = require('../models/crewschema');
var CreditModel = require('../models/creditschema');
var Note = require('../models/noteschema');
var NoteModel = Note.model;
var SheetModel = require('../models/sheetschema');
var StageSub = require('../models/stagesubschema');
var StageSubModel = StageSub.model

/*router.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "http://165.227.57.33:3000");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});*/

router.get('/dummy', function(req, res, next){

  res.json({
    success: false,
    sheets: {}
  })
})

router.get('/:id', function(req, res, next) {
  SheetModel.find({ _id: req.params.id }).exec()
  .then(function(sheet) {
    res.json({
      success: true,
      sheet: sheet
    })
  })
  .catch(function(error){
    console.log('Failed to find sheet')
    console.log(error)
    res.json({
      success: false,
      sheet: {}
    })
  })
})

router.post('/save', function(req, res, next){
  var data = JSON.parse(req.body.sheet)
  var stages = JSON.parse(JSON.stringify(data.stages))
  data.stages = []
  for (var stg in stages) {
    var notes = JSON.parse(JSON.stringify(stg.notes))
    var ids = []
    for (var nt in notes) {
      
    }
    data.stages.push(stg._id)
  }

  var stages = []
  for (var stg in data.stages) {
    let notes = []
    for (var nt in stg.notes) {
      if (nt._id[0] === '~') {
        nt._id = null
      }
      notes.push(NoteModel.hydrate(nt))
    }
    if (stg._id[0] === '~') {
      stg._id = null
    }
    stg.notes = notes
    stages.push(StageSubModel.hydrate(stg))
  }
  data.stages = stages
  SheetModel.hydrate(data)
  SheetModel.findOneAndUpdate({ _id: data._id}, data, { upsert: true}).exec()
  .then(function(sheet){
    res.json({
      success: true,
      id: sheet._id
    })
  })
  .catch(function(error){
    console.log('Sheet save failed')
    console.log(error)
    res.json({
      success: false
    })
  })
})

router.post('/newId', function(req, res, next){
  var num = req.body.amount
  var tmp = []
  for (var i = 0; i < num; i++) {
    tmp.push(mongoose.Types.ObjectId())
  }
  res.json({
    ids: tmp
  })
})

router.get('/list', function(req, res, next){
  SheetModel.find().exec()
  .then(function(sheets){
    res.json({
      success: true,
      sheets: sheets
    })
  })
  .catch(function(error){
    console.log('Failed to find sheets')
    console.log(error)
    res.json({
      success: false,
      sheets: {}
    })
  })
})

router.post('/add', function(req, res, next){
  var sheet1 = new SheetModel( req.body.sheet )

  sheet1.save()
  .then(function(result){
    res.json({
      success: true
    })
  })
  .catch(function(error){
    console.log('Failed to save with error: ')
    console.log(error)
    res.json({
      success: false
    })
  })
})

router.post('/remove', function(req, res, next){
  var Id1 = req.body.target

  SheetModel.deleteOne({ _id: Id1 }).exec()
  .then(function(result){
    res.json({
      success: true
    })
  })
  .catch(function(error){
    console.log(error)
    res.json({
      success: false
    })
  })
})

module.exports = router
