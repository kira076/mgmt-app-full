var express = require('express');
var router = express.Router();
var db = require('../db');
var moment = require('moment');
var CrewModel = require('../models/crewschema');
var ProjectModel = require('../models/projectschema');
var StageModel = require('../models/stageschema');
var CreditModel = require('../models/creditschema');
var NoteModel = require('../models/noteschema');

/*router.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "http://165.227.57.33:3000");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});*/

router.get('/dummy', function(req, res, next){
  let perPage = 6
  let page = parseInt(req.query.page) || 0
  let pages = 0
  let nextUrl = ''
  let prevUrl = ''

  res.json({
    success: true,
    creditlist: [
      {
        _id: 0,
        role: 'Director',
        credit_type: 'Director',
        name: 'Johnny Test'
      }
    ]
  })
})

router.get('/list', function(req, res, next){
  CreditModel.find().exec()
  .then(function(credits){
    res.json({
      success: true,
      creditlist: credits
    })
  })
  .catch(function(error){
    console.log('Failed to find credits')
    console.log(error)
    res.json({
      success: false,
      creditlist: {}
    })
  })
})

router.post('/add', function(req, res, next){
  var credit1 = new CreditModel({
    role: req.body.role,
    name: req.body.name,
    crew_id: req.body.crew_id
  })

  credit1.save()
  .then(function(result){
    res.json({
      success: true
    })
  })
  .catch(function(error){
    console.log('Failed to save with error: ')
    console.log(error)
    res.json({
      success: false
    })
  })
})

router.post('/remove', function(req, res, next){
  var Id1 = req.body.target

  CreditModel.deleteOne({ _id: Id1 }).exec()
  .then(function(result){
    res.json({
      success: true
    })
  })
  .catch(function(error){
    console.log(error)
    res.json({
      success: false
    })
  })
})

module.exports = router
