var express = require('express');
var router = express.Router();
var db = require('../db');
var moment = require('moment');
var CrewModel = require('../models/crewschema');
var ProjectModel = require('../models/projectschema');
var StageModel = require('../models/stageschema');
var CreditModel = require('../models/creditschema');
var NoteModel = require('../models/noteschema');

/*router.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "http://165.227.57.33:3000");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});*/

router.get('/dummy', function(req, res, next){
  let perPage = 6
  let page = parseInt(req.query.page) || 0
  let pages = 0
  let nextUrl = ''
  let prevUrl = ''

  res.json({
    success: true,
    stagelist: [
      {}
    ]
  })
})

router.get('/list', function(req, res, next){
  StageModel.find().exec()
  .then(function(stages){
    res.json({
      success: true,
      stagelist: stages
    })
  })
  .catch(function(error){
    console.log('Failed to find stages')
    console.log(error)
    res.json({
      success: false,
      stagelist: {}
    })
  })
})

router.post('/add', function(req, res, next){
  let proj = req.body.project
  let slot = req.body.slot

  var stage1 = new StageModel({
    project: proj,
    slot: slot,
    label: req.body.label,
    start_date: req.body.start_date,
    finish_date: req.body.finish_date,
    deadline: req.body.deadline,
    lead: req.body.lead,
    primary: req.body.primary,
    status: req.body.status
  })

  stage1.save()
  .then(function(stage) {
    return ProjectModel.findOneAndUpdate({ _id: proj }, {  }).exec()
  })
  .then(function(project) {
    let arr = project.stages
    if (arr.length < slot) {
      arr.fill(null, arr.length, slot)
      arr.push(stage1._id)
    } else (
      arr[slot] = stage1
    )
    project.set('stages', arr)
    return project.save()
  })
  .then(function(result){
    res.json({
      success: true
    })
  })
  .catch(function(error){
    console.log('Failed to save with error: ')
    console.log(error)
    res.json({
      success: false
    })
  })
})

router.post('/remove', function(req, res, next){
  var Id1 = req.body.target

  StageModel.deleteOne({ _id: Id1 }).exec()
  .then(function(result){
    res.json({
      success: true
    })
  })
  .catch(function(error){
    console.log(error)
    res.json({
      success: false
    })
  })
})

module.exports = router
