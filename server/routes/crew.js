var express = require('express');
var router = express.Router();
var db = require('../db');
var moment = require('moment');
var CrewModel = require('../models/crewschema');
var ProjectModel = require('../models/projectschema');
var StageModel = require('../models/stageschema');
var CreditModel = require('../models/creditschema');
var NoteModel = require('../models/noteschema');

/*router.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "http://165.227.57.33:3000");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});*/

router.get('/dummy', function(req, res, next){
  let perPage = 6
  let page = parseInt(req.query.page) || 0
  let pages = 0
  let nextUrl = ''
  let prevUrl = ''

  res.json({
    success: true,
    crewmembers: [
      {
        _id: 0,
        name: 'Johnny Test',
        interval: 'Weekly',
        queue: 0,
        completed: 0
      }
    ]
  })
})

router.get('/list', function(req, res, next){
  CrewModel.find().exec()
  .then(function(crew){
    res.json({
      success: true,
      crewmembers: crew
    })
  })
  .catch(function(error){
    console.log('Failed to find crew')
    console.log(error)
    res.json({
      success: false,
      crewmembers: {}
    })
  })
})

router.post('/add', function(req, res, next){
  var crew1 = new CrewModel({
    name: req.body.name,
    interval: req.body.interval
  })

  crew1.save()
  .then(function(result){
    res.json({
      success: true
    })
  })
  .catch(function(error){
    console.log('Failed to save with error: ')
    console.log(error)
    res.json({
      success: false
    })
  })
})

router.post('/remove', function(req, res, next){
  var Id1 = req.body.target

  CrewModel.deleteOne({ _id: Id1 }).exec()
  .then(function(result){
    res.json({
      success: true
    })
  })
  .catch(function(error){
    console.log(error)
    res.json({
      success: false
    })
  })
})

/*router.get('/list', function(req, res) {
  CrewModel.find(null, 'name role interval completed queue notes', function(err, crew){
    if(err){
      res.send(err);
    } else {
      res.render('crewlist', {
        "crewlist" : crew
      });
    }
  });
});

router.get('/member/:crewID', function(req, res){
  var Id1 = req.params.crewID;

  CrewModel.findOne({ _id: Id1 }, function(err, crew){
    if(err){
      res.send(err);
    } else {
      res.render('crewmember', {
        "data" : crew,
        "moment" : moment
      });
    }
  });
});

router.get('/new', function(req, res){
  res.render('newcrew', {title: 'Add Crew'});
});

router.get('/delete/:crewID', function(req, res){
  var Id1 = req.params.crewID;
  CrewModel.deleteOne({ _id: Id1 }, function(err){
    if(err){
      console.log(err);
    } else {
      res.render('crewdeleted');
    }
  });
});

router.post('/add', function(req, res){
  var crew1 = new CrewModel({
    name: req.body.crew,
    interval: req.body.interval
  });

  crew1.save(function(err){
    if(err){
      console.log(err);
    } else {
      res.redirect('list');
    }
  });
});*/

module.exports = router;
