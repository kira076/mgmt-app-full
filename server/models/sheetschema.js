var mongoose = require('mongoose');
var express = require('express');
var autopop = require('mongoose-autopopulate');
var uniquearray = require('mongoose-unique-array');
var stagesub = require('./stagesubschema.js')

var Schema = mongoose.Schema;

var SheetSchema = new Schema({
  title: { type: String, required: true, index: true, unique: true },
  start_date: { type: Date, required: true, index: true },
  finish_date: { type: Date },
  release_date: { type: Date},
  deadline: { type: Date },
  supervisor: { type: Schema.Types.ObjectId, ref: 'Credit', autopopulate: true },
  director: { type: Schema.Types.ObjectId, ref: 'Credit', autopopulate: true },
  other_credits: [{ type: Schema.Types.ObjectId, ref: 'Credit', autopopulate: true }],
  stages: [{ Schema.Types.ObjectId, ref: 'StageSub', autopopulate: true }]
});

SheetSchema.plugin(autopop);
// ProjectSchema.plugin(uniquearray);

var SheetModel = mongoose.model('Sheet', SheetSchema);

module.exports = SheetModel;
