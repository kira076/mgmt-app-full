var mongoose = require('mongoose');
var express = require('express');
var autopop = require('mongoose-autopopulate');
var uniquearray = require('mongoose-unique-array');

var Schema = mongoose.Schema;

var ProjectSchema = new Schema({
  title: { type: String, required: true, index: true, unique: true },
  start_date: { type: Date, required: true, index: true },
  finish_date: { type: Date },
  release_date: { type: Date},
  deadline: { type: Date },
  supervisor: { type: Schema.Types.ObjectId, ref: 'Credit', autopopulate: true },
  director: { type: Schema.Types.ObjectId, ref: 'Credit', autopopulate: true },
  other_credits: [{ type: Schema.Types.ObjectId, ref: 'Credit', autopopulate: true }],
  stages: [{ type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true }],
  stage1: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage2: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage3: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage4: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage5: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage6: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage7: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage8: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage9: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage10: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage11: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage12: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage13: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  stage14: { type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true},
  additional_stages: [{ type: Schema.Types.ObjectId, ref: 'Stage', autopopulate: true }],
  notes: [{ type: Schema.Types.ObjectId, ref: 'Note', autopopulate: true }]
});

ProjectSchema.pre('remove', function(next){
  var project = this;
  project.model('Stage').update({ project: project._id }, { $unset: { project: 1 } }, { multi: true }, next);
});

ProjectSchema.plugin(autopop);
ProjectSchema.plugin(uniquearray);

var ProjectModel = mongoose.model('Project', ProjectSchema);

module.exports = ProjectModel;
