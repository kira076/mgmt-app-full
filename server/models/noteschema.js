var mongoose = require('mongoose');
var express = require('express');
var autopop = require('mongoose-autopopulate');
var uniquearray = require('mongoose-unique-array');

var Schema = mongoose.Schema;

var NoteSchema = new Schema({
  parent: { type: Schema.Types.ObjectId, ref: 'StageSub', autopopulate: true },
  created: { type: Date },
  deadline: { type: Date },
  contents: { type: String }
});

NoteSchema.plugin(autopop);
// ProjectSchema.plugin(uniquearray);

var NoteModel = mongoose.model('Note', NoteSchema);

module.exports = {
  model: NoteModel,
  schema: NoteSchema
};
