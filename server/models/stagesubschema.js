var mongoose = require('mongoose');
var express = require('express');
var autopop = require('mongoose-autopopulate');
var uniquearray = require('mongoose-unique-array');

var Schema = mongoose.Schema;

var StageSchema = new Schema({
  parent: { type: Schema.Types.ObjectId, ref: 'Sheet' },
  slot: { type: Number },
  label: { type: String },
  start_date: { type: Date, index: true }, //req
  finish_date: { type: Date },
  deadline: { type: Date },
  status: { type: Number},
  lead: { type: Schema.Types.ObjectId, ref: 'Credit', autopopulate: true },
  primary: { type: Schema.Types.ObjectId, ref: 'Credit', autopopulate: true }, //req
  other_credits: [{ type: Schema.Types.ObjectId, ref: 'Credit', autopopulate: true }],
  notes: [{ type: Schema.Types.ObjectId, ref: 'Note', autopopulate: true }],
  flags: [{  }],
  checks: [{  }]
});

StageSchema.plugin(autopop);
// ProjectSchema.plugin(uniquearray);

var StageSubModel = mongoose.model('StageSub', StageSchema);

module.exports = {
  model: StageSubModel,
  schema: StageSchema
};
