const getAllLists = ({ dispatch }) => {
  dispatch('$_crew/getList')
  dispatch('$_credits/getList')
  dispatch('$_projects/getList')
  dispatch('$_stages/getList')
}

export default {
  getAllLists
}
