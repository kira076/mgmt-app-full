const statusColors = state => state.statusColors

const stageTypes = state => state.stageTypes

const stageStatuses = state => state.stageStatuses

const trackerSlots = state => state.trackerSlots

const defaultLabels = state => state.defaultLabels

export default {
  statusColors,
  stageTypes,
  stageStatuses,
  trackerSlots,
  defaultLabels
}
