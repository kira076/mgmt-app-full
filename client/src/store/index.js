import Vue from 'vue'
import Vuex from 'vuex'
import actions from './rootActions.js'
import getters from './rootGetters.js'

import Credits from '@/modules/credits/_store'
import Crew from '@/modules/crew/_store'
import Projects from '@/modules/projects/_store'
import Stages from '@/modules/stages/_store'
import Tracker from '@/modules/tracker/_store'
import Sheet from '@/modules/sheets/_store'

Vue.use(Vuex)

const state = {
  statusColors: [
    { id: 0, color: 'blank' },
    { id: 1, color: 'awaiting' },
    { id: 2, color: 'assigned' },
    { id: 3, color: 'w_artist' },
    { id: 4, color: 'w_director' },
    { id: 5, color: 'w_supervisor' },
    { id: 6, color: 'w_ross' },
    { id: 7, color: 'finished' },
    { id: 8, color: 'notapplicable' }
  ],
  stageStatuses: [
    { id: 0, text: 'Blank' },
    { id: 1, text: 'Awaiting Assign' },
    { id: 2, text: 'Assigned (Not Started)' },
    { id: 3, text: 'With Artist' },
    { id: 4, text: 'With Director' },
    { id: 5, text: 'With Supervisor' },
    { id: 6, text: 'With Ross' },
    { id: 7, text: 'Finished' },
    { id: 8, text: 'Not Applicable' }
  ],
  trackerSlots: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13], // Index values in trackerSlots correspond to indices of defaultLabels - i.e. trackerSlots[2] should be labeled with defaultLabels[2]
  defaultSlots: [14, 1, 2, 15, 4, 16, 17, 9, 18, 19, 13],
  defaultLabels: [ // Index values match trackerSlots indices
    'Outline', // 0
    'Storyboard', // 1
    'Animation', // 2
    'Color', // 3
    'Thumbnail', // 4
    'Pencil1', // 5
    'Retime1', // 6
    'Voice', // 7
    'Retime2', // 8
    'Music', // 9
    'Retime3', // 10
    'SFX', // 11
    'Pencil2', // 12
    'Export' // 13
  ],
  stageTypes: [
    { index: 0, name: 'Outline' },
    { index: 1, name: 'Storyboard' },
    { index: 2, name: 'Animation' },
    { index: 3, name: 'Color' },
    { index: 4, name: 'Thumbnail' },
    { index: 5, name: 'Pencil1' },
    { index: 6, name: 'Retime1' },
    { index: 7, name: 'Voice' },
    { index: 8, name: 'Retime2' },
    { index: 9, name: 'Music' },
    { index: 10, name: 'Retime3' },
    { index: 11, name: 'SFX' },
    { index: 12, name: 'Pencil2' },
    { index: 13, name: 'Export' },
    { index: 14, name: 'Script' },
    { index: 15, name: 'Color Design' },
    { index: 16, name: 'Pencil 1/Color' },
    { index: 17, name: 'Voice/SFX 1' },
    { index: 18, name: 'SFX 2+' },
    { index: 19, name: 'Pencil 2+/Comp' }
  ]
}

export default new Vuex.Store({
  actions,
  state,
  getters,
  modules: {
    $_credits: Credits,
    $_crew: Crew,
    $_projects: Projects,
    $_stages: Stages,
    $_tracker: Tracker,
    $_sheet: Sheet
  }
})
