// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import VueMoment from 'vue-moment'
import router from './router'
import App from './App'
import './stylus/main.styl'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import store from './store'

Vue.config.productionTip = false

Vue.use(VueMoment)

Vue.use(Vuetify, {
  theme: {
    primary: '#2979FF',
    secondary: '#0D47A1',
    accent: '#FF8F00',
    error: '#f44336',
    warning: '#ffeb3b',
    info: '#2196f3',
    success: '#4caf50',
    blank: '#FFFF99',
    awaiting: '#66FFFF',
    assigned: '#DDDDDD',
    w_artist: '#FF66CC',
    w_director: '#FFCC00',
    w_supervisor: '#FF9999',
    w_ross: '#FF6666',
    finished: '#00FF66',
    notapplicable: '#808080'
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
