import axios from 'axios'

export default () => {
  return axios.create({
    baseURL: 'http://165.227.57.33/api'
  })
}
