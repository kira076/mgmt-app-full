import CreditDialog from '@/components/CreditDialog'
import NotesDialog from '@/components/NotesDialog'
import RemoveDialog from '@/components/RemoveDialog'
import Crew from '@/modules/crew'
import Projects from '@/modules/projects'
import Stages from '@/modules/stages'
import Credits from '@/modules/credits'
import Tracker from '@/modules/tracker'
import App from '@/App'

var Apps = {
  App,
  Tracker,
  Credits,
  Stages,
  Projects,
  Crew,
  CreditDialog,
  NotesDialog,
  RemoveDialog
}

export { Apps }
