import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Crew from '@/modules/crew'
import Projects from '@/modules/projects'
import Stages from '@/modules/stages'
import Credits from '@/modules/credits'
import Tracker from '@/modules/tracker'
import Sheets from '@/modules/sheets'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/crew',
      name: 'Crew',
      component: Crew
    },
    {
      path: '/projects',
      name: 'Projects',
      component: Projects
    },
    {
      path: '/stages',
      name: 'Stages',
      component: Stages
    },
    {
      path: '/credits',
      name: 'Credits',
      component: Credits
    },
    {
      path: '/tracker',
      name: 'Production Tracker',
      component: Tracker
    },
    {
      path: '/sheet/:id',
      name: 'Production Sheet',
      component: Sheets
    }
  ]
})
