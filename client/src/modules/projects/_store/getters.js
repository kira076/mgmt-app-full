const projects = state => state.projects

const viewDialog = state => state.viewDialog

const working = state => state.working

const target = state => state.target

const info = state => state.info

const failErr = state => state.failErr

const storeKey = state => state.storeKey

export default {
  projects,
  target,
  viewDialog,
  info,
  working,
  failErr,
  storeKey
}
