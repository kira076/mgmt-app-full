import _ from 'lodash'

const PROJECTS_UPDATED = (state, list) => {
  state.projects = list
}

const ERRORED = (state, error) => {
  state.failErr.status = true
  state.failErr.error = error
}

const MAKE_TARGET = (state, target) => {
  state.target = target
  if (_.isEmpty(target)) {
    state.info.remove.question = 'Remove a project?'
  } else {
    state.info.remove.question = 'Remove ' + target.title + ' (Id: ' + target._id + ')?'
  }
}

const TOGGLE_DIALOG = (state, { dialog, display }) => {
  switch (dialog) {
    case 'credits':
      state.viewDialog.credits = display
      break
    case 'notes':
      state.viewDialog.notes = display
      break
    case 'remove':
      state.viewDialog.remove = display
      break
    case 'add':
      state.viewDialog.add = display
      break
  }
}

const CLOSE_ERROR = (state) => {
  state.failed.status = false
  state.failed.error = {}
}

const TOGGLE_WORKING = (state, setting) => {
  state.working = setting
}

export default {
  PROJECTS_UPDATED,
  ERRORED,
  MAKE_TARGET,
  TOGGLE_DIALOG,
  CLOSE_ERROR,
  TOGGLE_WORKING
}
