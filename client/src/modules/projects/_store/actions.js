import api from '../_api'

const getList = (context) => {
  api.getProjectList()
    .then((response) => {
      context.commit('PROJECTS_UPDATED', response.data.projects)
    })
    .catch((error) => {
      console.log(error)
      context.commit('ERRORED', error)
    })
}

const dialog = ({ commit }, { dialog, display, target }) => {
  commit('MAKE_TARGET', target)
  commit('TOGGLE_DIALOG', { dialog: dialog, display: display })
}

const setTarget = ({ commit }, target) => {
  commit('MAKE_TARGET', target)
}

const remove = (context) => {
  context.commit('TOGGLE_WORKING', true)
  api.removeProject(context.state.target._id)
    .then((response) => {
      context.commit('TOGGLE_WORKING', false)
      context.dispatch('dialog', { dialog: 'remove', target: {}, display: false })
      if (response.data.success) {
        context.dispatch('getList')
      } else {
        context.commit('ERRORED', response.data.error)
      }
    })
    .catch((error) => {
      context.commit('TOGGLE_WORKING', false)
      context.dispatch('dialog', { dialog: 'remove', target: {}, display: false })
      context.commit('ERRORED', error)
      console.log(error)
    })
}

const closeFailed = ({ commit }) => {
  commit('CLOSE_ERROR')
}

const failed = ({ commit }, error) => {
  commit('ERRORED', error)
}

const add = (context, payload) => {
  context.commit('TOGGLE_WORKING', true)
  api.addProject(payload)
    .then((response) => {
      context.commit('TOGGLE_WORKING', false)
      context.dispatch('dialog', { dialog: 'add', target: {}, display: false })
      if (response.data.success) {
        context.dispatch('getList')
      } else {
        context.commit('ERRORED', response.data.error)
      }
    })
    .catch((error) => {
      context.commit('TOGGLE_WORKING', false)
      context.dispatch('dialog', { dialog: 'add', target: {}, display: false })
      context.commit('ERRORED', error)
      console.log(error)
    })
}

export default {
  getList,
  dialog,
  setTarget,
  remove,
  closeFailed,
  failed,
  add
}
