import http from '@/config/API.js'

export default {
  getProjectList () {
    return http().get('/projects/list')
  },
  getProjectDummy () {
    return http().get('/projects/dummy')
  },
  removeProject (target) {
    return http().post('/projects/remove', {
      target: target
    })
  },
  addProject (payload) {
    return http().post('/projects/add', payload)
  }
}
