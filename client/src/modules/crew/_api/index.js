import http from '@/config/API.js'

export default {
  getCrewList () {
    return http().get('/crew/list')
  },
  removeCrew (target) {
    return http().post('/crew/remove', {
      target: target
    })
  },
  addCrew ({ name, interval }) {
    return http().post('/crew/add', {
      name: name,
      interval: interval
    })
  }
}
