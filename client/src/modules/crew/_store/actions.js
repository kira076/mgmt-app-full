import api from '../_api'

const getList = (context) => {
  api.getCrewList()
    .then((response) => {
      context.commit('CREW_UPDATED', response.data.crewmembers)
    })
    .catch((e) => {
      console.log(e)
      context.commit('CREW_ERRORED', e)
    })
}

const dialog = ({ commit }, { dialog, display, target }) => {
  commit('MAKE_TARGET', target)
  commit('TOGGLE_DIALOG', { dialog: dialog, display: display })
}

const setTarget = ({ commit }, target) => {
  commit('MAKE_TARGET', target)
}

const remove = (context) => {
  context.commit('TOGGLE_WORKING', true)
  api.removeCrew(context.state.target._id)
    .then((response) => {
      context.commit('TOGGLE_WORKING', false)
      context.dispatch('dialog', { dialog: 'remove', target: {}, display: false })
      if (response.data.success) {
        context.dispatch('getList')
      } else {
        context.commit('CREW_ERRORED', response.data.error)
      }
    })
    .catch((error) => {
      context.commit('TOGGLE_WORKING', false)
      context.dispatch('dialog', { dialog: 'remove', target: {}, display: false })
      context.commit('CREW_ERRORED', error)
      console.log(error)
    })
}

const closeFailed = ({ commit }) => {
  commit('CLOSE_ERROR')
}

const failed = ({ commit }, error) => {
  commit('CREW_ERRORED', error)
}

const add = (context, { name, interval }) => {
  context.commit('TOGGLE_WORKING', true)
  api.addCrew({ name: name, interval: interval })
    .then((response) => {
      context.commit('TOGGLE_WORKING', false)
      context.dispatch('dialog', { dialog: 'add', target: {}, display: false })
      if (response.data.success) {
        context.dispatch('getList')
      } else {
        context.commit('CREW_ERRORED', response.data.error)
      }
    })
    .catch((error) => {
      context.commit('TOGGLE_WORKING', false)
      context.dispatch('dialog', { dialog: 'add', target: {}, display: false })
      context.commit('CREW_ERRORED', error)
      console.log(error)
    })
}

export default {
  getList,
  dialog,
  setTarget,
  remove,
  closeFailed,
  failed,
  add
}
