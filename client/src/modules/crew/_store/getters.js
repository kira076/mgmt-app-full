const crewmembers = state => state.crewmembers

const target = state => state.target

const viewDialog = state => state.viewDialog

const info = state => state.info

const working = state => state.working

const failErr = state => state.failErr

const fullStore = state => state

const storeKey = state => state.storeKey

export default {
  crewmembers,
  target,
  viewDialog,
  info,
  working,
  failErr,
  fullStore,
  storeKey
}
