import actions from './actions.js'
import getters from './getters.js'
import mutations from './mutations.js'

const state = {
  crewmembers: [],
  target: {},
  viewDialog: {
    credits: false,
    notes: false,
    remove: false,
    add: false
  },
  failErr: {
    status: false,
    error: {}
  },
  info: {
    remove: {
      question: 'Remove a crew member?',
      ycolor: 'error',
      ncolor: ''
    }
  },
  working: false,
  storeKey: '$_crew'
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}
