import http from '@/config/API.js'

export default {
  getCreditList () {
    return http().get('/credits/list')
  },
  getCreditDummy () {
    return http().get('/credits/dummy')
  },
  removeCredit (target) {
    return http().post('/credits/remove', {
      target: target
    })
  },
  addCredit (payload) {
    return http().post('/credits/add', payload)
  }
}
