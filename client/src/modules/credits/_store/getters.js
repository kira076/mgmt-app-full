const creditlist = state => state.creditlist

const viewDialog = state => state.viewDialog

const working = state => state.working

const target = state => state.target

const info = state => state.info

const failErr = state => state.failErr

const storeKey = state => state.storeKey

export default {
  creditlist,
  target,
  viewDialog,
  info,
  working,
  failErr,
  storeKey
}
