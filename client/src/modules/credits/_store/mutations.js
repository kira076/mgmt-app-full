import _ from 'lodash'

const CREDITS_UPDATED = (state, list) => {
  state.creditlist = list
}

const ERRORED = (state, error) => {
  state.failErr.status = true
  state.failErr.error = error
}

const MAKE_TARGET = (state, target) => {
  state.target = target
  if (_.isEmpty(target)) {
    state.info.remove.question = 'Remove a credit?'
  } else {
    state.info.remove.question = 'Remove ' + target.role + ': ' + target.name + ' (Id: ' + target._id + ')?'
  }
}

const TOGGLE_DIALOG = (state, { dialog, display }) => {
  switch (dialog) {
    case 'notes':
      state.viewDialog.notes = display
      break
    case 'remove':
      state.viewDialog.remove = display
      break
    case 'add':
      state.viewDialog.add = display
      break
  }
}

const CLOSE_ERROR = (state) => {
  state.failErr.status = false
  state.failErr.error = {}
}

const TOGGLE_WORKING = (state, setting) => {
  state.working = setting
}

export default {
  CREDITS_UPDATED,
  ERRORED,
  MAKE_TARGET,
  TOGGLE_DIALOG,
  CLOSE_ERROR,
  TOGGLE_WORKING
}
