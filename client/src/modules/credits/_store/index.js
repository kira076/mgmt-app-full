import actions from './actions.js'
import getters from './getters.js'
import mutations from './mutations.js'

const state = {
  creditlist: [],
  viewDialog: {
    notes: false,
    remove: false,
    add: false
  },
  target: {},
  failErr: {
    status: false,
    error: {}
  },
  info: {
    remove: {
      question: 'Remove a credit?',
      ycolor: 'error',
      ncolor: ''
    }
  },
  working: false,
  storeKey: '$_credits'
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}
