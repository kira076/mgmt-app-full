import actions from './actions.js'
import getters from './getters.js'
import mutations from './mutations.js'

const state = {
  storeKey: '$_tracker'
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}
