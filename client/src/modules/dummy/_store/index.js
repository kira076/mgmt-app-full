import actions from './actions.js'
import getters from './getters.js'
import mutations from './mutations.js'

const state = {
  storeKey: '' // $_name
  // Other pieces of state data go here
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}
