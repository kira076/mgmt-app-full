import http from '@/config/API.js'

export default {
  getStageList () {
    return http().get('/stages/list')
  },
  getStageDummy () {
    return http().get('/stages/dummy')
  },
  removeStage (target) {
    return http().post('/stages/remove', {
      target: target
    })
  },
  addStage (payload) {
    return http().post('/stages/add', payload)
  }
}
