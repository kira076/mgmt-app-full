const stagelist = state => state.stagelist

const viewDialog = state => state.viewDialog

const working = state => state.working

const target = state => state.target

const info = state => state.info

const failErr = state => state.failErr

const storeKey = state => state.storeKey

const stageTypes = rootState => rootState.stageTypes

const statuses = rootState => rootState.stageStatuses

export default {
  stagelist,
  target,
  viewDialog,
  info,
  working,
  failErr,
  storeKey,
  stageTypes,
  statuses
}
