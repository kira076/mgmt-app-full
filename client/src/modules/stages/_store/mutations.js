import _ from 'lodash'

const STAGES_UPDATED = (state, list) => {
  state.stagelist = list
}

const ERRORED = (state, error) => {
  state.failErr.status = true
  state.failErr.error = error
}

const MAKE_TARGET = (state, target) => {
  state.target = target
  if (_.isEmpty(target)) {
    state.info.remove.question = 'Remove a stage?'
  } else {
    if (_.isEmpty(target.project)) {
      state.info.remove.question = 'Remove' + target.stage_name + '(Id: ' + target._id + ') of a removed project?'
    } else {
      state.info.remove.question = 'Remove ' + target.project.title + ' - ' + target.stage_name + ' (Id: ' + target._id + ')?'
    }
  }
}

const TOGGLE_DIALOG = (state, { dialog, display }) => {
  switch (dialog) {
    case 'credits':
      state.viewDialog.credits = display
      break
    case 'notes':
      state.viewDialog.notes = display
      break
    case 'remove':
      state.viewDialog.remove = display
      break
    case 'add':
      state.viewDialog.add = display
      break
  }
}

const CLOSE_ERROR = (state) => {
  state.failed.status = false
  state.failed.error = {}
}

const TOGGLE_WORKING = (state, setting) => {
  state.working = setting
}

export default {
  STAGES_UPDATED,
  ERRORED,
  MAKE_TARGET,
  TOGGLE_DIALOG,
  CLOSE_ERROR,
  TOGGLE_WORKING
}
