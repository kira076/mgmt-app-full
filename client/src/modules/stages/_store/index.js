import actions from './actions.js'
import getters from './getters.js'
import mutations from './mutations.js'

const state = {
  stagelist: [],
  viewDialog: {
    credits: false,
    notes: false,
    remove: false,
    add: false
  },
  target: {},
  failErr: {
    status: false,
    error: {}
  },
  info: {
    remove: {
      question: 'Remove a stage?',
      ycolor: 'error',
      ncolor: ''
    }
  },
  working: false,
  storeKey: '$_stages'
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}
