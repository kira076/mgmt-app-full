import http from '@/config/API.js'

export default {
  getSheetById (id) {
    return http().get('/sheets/' + id)
  },
  saveSheet (sheet) {
    return http().post('/sheets/save', {
      sheet: sheet
    })
  },
  removeSheet (target) {
    return http().post('/sheets/remove', {
      target: target
    })
  },
  addSheet (payload) {
    return http().post('/sheets/add', payload)
  },
  getNewId (num) {
    return http().post('/sheets/newId', {
      amount: num
    })
  }
}
