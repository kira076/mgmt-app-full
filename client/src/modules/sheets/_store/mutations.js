
const UPDATE_SHEET = (state, sheet) => {
  state.sheetModel = sheet
}

const PUSH_IDS = (state, ids) => {
  let tmp = state.newIds
  state.newIds = tmp.concat(ids)
}

const ERRORED = (state, error) => {
  state.failed.status = true
  state.failed.info = error
}

export default {
  UPDATE_SHEET,
  ERRORED
}
