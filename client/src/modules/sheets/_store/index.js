import actions from './actions.js'
import getters from './getters.js'
import mutations from './mutations.js'

const state = {
  sheetModel: {},
  failed: {
    status: false,
    info: {}
  },
  defaultStage: { _id: -1, label: 'Test', slot: '0', start_date: 'YYYY-MM-DD', finish_date: 'YYYY-MM-DD', lead: 'Select One', primary: 'Select One', status: 0, notes: [] },
  storeKey: '$_sheet' // $_name
  // Other pieces of state data go here
}

export default {
  namespaced: true,
  state,
  mutations,
  getters,
  actions
}
