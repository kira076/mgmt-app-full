const defaultStages = (state, getters, rootState) => {
  let d = []
  for (var i = 0; i < rootState.defaultSlots.length; i++) {
    let dummy = JSON.parse(JSON.stringify(state.defaultStage))
    dummy._id = state.newIds.pop()
    dummy.slot = rootState.defaultSlots[i]
    d.push(dummy)
  }
  return d
}

export default {
  defaultStages
}
