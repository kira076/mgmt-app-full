import api from '../_api'
import router from '@/router'
import _ from 'lodash'

const getSheet = (context, id) => {
  if (id === 'new' || _.isEmpty(id)) {
    let id = context.state.newIds.pop()
    let date = 'YYYY-MM-DD'
    let cred = { _id: -1, name: 'NONE' }
    let creds = [].push(cred)
    let d = []
    for (var i = 0; i < context.rootState.defaultSlots.length; i++) {
      let dummy = JSON.parse(JSON.stringify(state.defaultStage))
      dummy.parent = id
      dummy._id = state.newIds.pop()
      dummy.slot = rootState.defaultSlots[i]
      d.push(dummy)
    }
    let neu = { _id: id, title: 'Project', start_date: date, finish_date: date, deadline: date, release_date: date, director: cred, supervisor: cred, other_credits: creds, stages: d }
    context.commit('UPDATE_SHEET', neu)
    context.dispatch('$_sheet/getIds', context.rootState.defaultSlots.length + 1)
  } else {
    api.getSheetById(id)
      .then((response) => {
        context.commit('UPDATE_SHEET', response.data.sheet)
      })
      .catch((error) => {
        console.log(error)
        context.commit('ERRORED', error)
      })
  }
}

const getIds = (context, num) => {
  api.getNewId(num)
    .then((response) => {
      context.commit('PUSH_IDS', response.data.ids)
    })
    .catch((error) => {
      console.log(error)
      context.commit('ERRORED', error)
    })
}

const popId = (context) => {
  return context.state.newIds.pop()
  context.dispatch('$_sheet/getIds', 1)
}

const saveSheet = (context, sheet) => {
  api.saveSheet(sheet)
    .then((response) => {
      if (response.data.success === true) {
        router.push('/sheet/' + sheet._id)
      } else if (response.data.success === false) {
        alert('SAVE FAILED')
      }
    })
    .catch((error) => {
      console.log(error)
      context.commit('ERRORED', error)
    })
}

const saveStage = ({ state }, stage) => {
  let sheet = state.sheetModel
  let original = state.sheetModel.stages.find(st => st._id === stage._id)
  let ind = sheet.stages.indexOf(original)
  sheet.stages.splice(ind, 1, stage)
}

const updateStage = ({ state }, payload) => {
  let stage = payload.stage
  let sheet = state.sheetModel
  let original = state.sheetModel.stages.find(st => st._id === stage._id)
  let ind = sheet.stages.indexOf(original)
  console.log('Update |', 'Stage:', stage, 'Original:', original, 'Index:', ind)
  if (payload.action === 'save') {
    sheet.stages.splice(ind, 1, stage)
  } else if (payload.action === 'remove') {
    sheet.stages.splice(ind, 1)
  }
}

export default {
  getSheet,
  getIds,
  saveSheet,
  saveStage,
  updateStage
}
